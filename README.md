# velox-demo

This project is a demo of a full shop.

## Hardware Requirements for Docker

Memory (minimum): 6 GB

## Start the service:

Execute locally with: Docker Compose

`export SPRING_SECURITY_OAUTH2_RESOURCESERVER_OPAQUETOKEN_CLIENT_SECRET="<oauth client secret>"`\
`docker-compose stop && docker-compose rm -fv && docker-compose pull && docker-compose --compatibility --env-file .env.local up -d --build --force-recreate --remove-orphans`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
Back-office interface is available at [http://localhost:3001/backoffice](http://localhost:3001/backoffice).

## Users and credentials

To create a normal user just click on login and register yourself.\
To get access to the admin interface and to run the initial-data import you will find the crednetials in the documentation:\
[ZITADEL accounts](https://gitlab.com/velox-shop/documentation#zitadel-accounts)


## Importing Demo Data

To have some sample products, with prices and availabilities in your local environment, you can import our initia-data.

`./gradlew initialData -PimportEnvironment="veloxDemoDeployLocal" -Ptoken="<oauth authentication token>"`
